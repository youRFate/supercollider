use rand;
use rand::prelude::*;
use crc32c_hw;
use std::time::Instant;
use std::thread;
use std::sync::{Mutex, Arc};

fn main() {
    const NUM_BYTES : usize = 10 * 1024 * 1024 ;
    const CORRECT_CRC : u32 = 1214729159;
    const NUM_THREADS : usize = 8;
    const NUM_ITTERATIONS : usize = 1000;

    // prepare arrays and random generator
    let mut data_vec = vec![0u8; NUM_BYTES];
    let mut rng = rand::thread_rng();
    // prepare 10 meg of random data
    rng.fill_bytes(&mut data_vec);
    // calculate crc
    let crc = crc32c_hw::compute(&data_vec);
    // append crc
    data_vec.extend(&crc.to_le_bytes());
    // make it immutable
    let data_vec = data_vec;
    // some pre-checking
    println!("Checking CRC validity of test data... {}", check_crc(&data_vec, CORRECT_CRC));
    // bechmarking
    let now = Instant::now();
    // array of bit offsets
    let mut offset_arr : [(usize, u32); NUM_ITTERATIONS*NUM_THREADS] = [(0,0); NUM_ITTERATIONS*NUM_THREADS];
    // so ugly.. use some kind of map here...
    for idx in 0..offset_arr.len() {
        offset_arr[idx].0 = rng.gen_range(0, NUM_BYTES);
        offset_arr[idx].1 = rng.gen_range(0, 7);
    }
    // make offsets immutable
    let offset_arr = offset_arr;
    // mutex counter
    let counter = Arc::new(Mutex::new(0));
    let mut handles = vec![];
    println!("Running {} threads with {} itterations of {} bytes...", NUM_THREADS, NUM_ITTERATIONS, NUM_BYTES);
    for idx_thread in 0..NUM_THREADS {
        let vec_c1 = data_vec.clone();
        let counter = Arc::clone(&counter);
        let handle = thread::spawn(move || {
            for idx in 0..NUM_ITTERATIONS {
                let vec_c = vec_c1.clone();
                if corrupt_and_check(
                    vec_c,
                    CORRECT_CRC,
                    offset_arr[idx_thread*idx].0,
                    offset_arr[idx_thread*idx].1
                ) {
                    let mut num = counter.lock().unwrap();
                    *num +=1;
                }
            }
        });
        handles.push(handle);
    }
    for handle in handles {
        handle.join().unwrap();
    }
    let elapsed = now.elapsed();
    let sec = (elapsed.as_secs() as f64) + (elapsed.subsec_nanos() as f64 / 1000_000_000.0);
    let total_data = NUM_BYTES*NUM_THREADS*NUM_ITTERATIONS;
    let gb_per_second = (total_data as f64) / 1E9 / sec;
    println!("Speed: {:.3} GB/s, found {} collisions.", gb_per_second, *counter.lock().unwrap());
}

fn corrupt_and_check(mut data_vec: Vec<u8>, correct_crc: u32, c_off_byte: usize, c_off_bit: u32) -> bool {
    // mask for corrupting a single bit
    let mask: u8 = 1 << c_off_bit;
    // flip the bit
    data_vec[c_off_byte] = data_vec[c_off_byte] ^ mask;
    // return the result
    check_crc(&data_vec, correct_crc)
}

fn check_crc(data_vec: &Vec<u8>, correct_crc: u32) -> bool {
    let crc = crc32c_hw::compute(&data_vec);
    crc == correct_crc
}
