# crc32 collision finder experiment

Some dabbling in rust. This program will create 10MB of random data and add a
crc32 checksum to it. It will then spawn a bunch of threads, each of wich will
corrupt a random bit in that data and check to see if the crc32 check detects it.

This was mostly done as a first execise in rust, reaches about 7.3GB/s of
throughput on my i7 6700k.